CREATE DATABASE `testing-codeingniter`;


CREATE TABLE `jugadores` (
  `jugador_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`jugador_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1


CREATE TABLE `partidas` (
  `partida_id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(8) DEFAULT NULL,
  `jugador_ppal_id` int(11) DEFAULT NULL,
  `jugador_sec_id` int(11) DEFAULT NULL,
  `ganador_id` int(11) NOT NULL,
  `start_jugar_ppal` tinyint(1) DEFAULT NULL,
  `start_jugar_sec` tinyint(1) DEFAULT NULL,
  `estatus` enum('TERMINADA','EN PROCESO','INICIADA','CREADA') DEFAULT 'CREADA',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`partida_id`),
  KEY `fk_jufgador_sec_id` (`jugador_sec_id`),
  KEY `fk_jufgador_ppal_id` (`jugador_ppal_id`),
  CONSTRAINT `fk_jufgador_ppal_id` FOREIGN KEY (`jugador_ppal_id`) REFERENCES `jugadores` (`jugador_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1


CREATE TABLE `partida_detalle` (
  `partida_detalle_id` int(11) NOT NULL AUTO_INCREMENT,
  `partida_id` int(11) DEFAULT NULL,
  `jugador_id` int(11) DEFAULT NULL,
  `detalle_movimiento` varchar(11) DEFAULT NULL,
  `simbolo` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`partida_detalle_id`),
  KEY `fk_partida_id` (`partida_id`) USING BTREE,
  KEY `fk_jugador_id` (`jugador_id`) USING BTREE,
  CONSTRAINT `fk_id_jugador` FOREIGN KEY (`jugador_id`) REFERENCES `jugadores` (`jugador_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1
