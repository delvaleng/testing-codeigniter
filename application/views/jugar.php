<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/public/style.css">

<div class="container">
	<br><br>

	<section class="content-header">
		<div class="pull-left"  style="margin-top: -10px;margin-bottom: 5px"><b> EQUIPO A&nbsp;&nbsp;&nbsp;&nbsp;<b>
			<br>
			<span class="glyphicon glyphicon-user"></span> <label id="html_jugadora"></label>
		</div>
		<div class="pull-right" style="margin-top: -10px;margin-bottom: 5px"><b> EQUIPO B&nbsp;&nbsp;&nbsp;&nbsp;<b>
			<br>
			<span class="glyphicon glyphicon-user"></span> <label id="html_jugadorb"></label>
		</div>
	</section>

	<span2 id="turn"><label id="estatus"><img src="<?php echo base_url(); ?>backend/images/loader.gif" width="40px" height="40px"></label></span2>
	<span>N&deg; de Jugada: <?php echo $codigo ?></span2>

		<!-- Inputs de apoyo -->
		<input type="hidden" class="form-control" id="partida_id"   name="partida_id"   value="<?php echo $partida_id  ?>"                  placeholder="partida_id">
		<input type="hidden" class="form-control" id="jugador_id"   name="jugador_id"   value="<?php echo $jugador_id  ?>"                  placeholder="jugador_id">
		<input type="hidden" class="form-control" id="invitado"     name="invitado"     value="<?php echo $partidas->jugador_sec_id  ?>"    placeholder="invitado">
		<input type="hidden" class="form-control" id="estatusform"  name="estatusform"  value="<?php echo $estatus      ?>"                 placeholder="estatus">
		<input type="hidden" class="form-control" id="ganador_id"   name="ganador_id"   value="<?php echo $ganador_id  ?>"                  placeholder="ganador_id">
		<input type="hidden" class="form-control" id="jugador_type" name="jugador_type" value="<?php echo $jugador_type  ?>" >
		<input type="hidden" class="form-control" id="turno"        name="turno">
		<input type="hidden" class="form-control" id="num"          name="num">

	<!-- Casillas de tic tac toe -->
	<div class="container2" id="main">
       <div class="box" style="border-left: 0; border-top: 0"     id="box1"></div>
       <div class="box" style="border-top:  0"                    id="box2"></div>
       <div class="box" style="border-top:  0; border-right: 0"   id="box3"></div>
       <div class="box" style="border-left: 0"                    id="box4"></div>
       <div class="box"                                           id="box5"></div>
       <div class="box" style="border-right: 0"                   id="box6"></div>
       <div class="box" style="border-left:  0; border-bottom: 0" id="box7"></div>
       <div class="box" style="border-bottom:0"                   id="box8"></div>
       <div class="box" style="border-right: 0; border-bottom: 0" id="box9"></div>
     </div>
	 <hr>
	 <!-- Iniciar partida nueva una vez terminado el juego -->
	 <div class="form-group" align="center" id="partidaNueva" style="display:none">
	   <a type="button" class="btn btn-success btn-lg btn-block" href="<?php echo base_url();?>index.php/partida">&nbsp;&nbsp;Iniciar otra partida &nbsp;&nbsp;</span></a>
	 </div>

	<p>
	<strong>Leyenda:</strong><br />
		X - EQUIPO A<br />
		O - EQUIPO B<br />
	</p>

</div> <!-- /container -->


<script type="text/javascript">

	$(document).ready(function(){
		setInterval(function() {
                  getMovimiento()
                }, 3000);
	});

	var turn  = document.getElementById("turn"),
	    boxes = document.querySelectorAll("#main div"), X_or_O = 0;

	for (var i = 0; i < boxes.length; i++) {
		boxes[i].onclick = function () {

			var jugadorType  = $("#jugador_type").val();
			var turno        = $("#turno").val();
			var num          = $("#num").val();


			if($("#invitado").val() == null ){
				alert("Espera aún no llega tu usuario invitado");
				return false;
			}
			if($("#jugador_type").val() !== $("#turno").val() ){
				alert("Espera aún no juega tu invitado");
				return false;
			}

			if (this.innerHTML !== "X" && this.innerHTML !== "O" ) {

				//JUEGA EL JUGADOR 1 - Aqui empieza a marcar x

				if (num % 2 === 0) {
						this.innerHTML = "X";
						turn.innerHTML = "JUGADOR 2";
						$("#turno").val("O");
						getWinner();
						insertMovimiento(this.id, 'X');

				}


				//JUEGA EL JUGADOR 2 - Aqui empieza a marcar o
				else {
						this.innerHTML = "O";
						turn.innerHTML = "JUGADOR 1";
						$("#turno").val("X");

						getWinner();
						insertMovimiento(this.id,'O');

				}

			}

		};
	}


	// Obtenemos movimientos realizados
	function getMovimiento()
	{

		$.ajax({
		  url: "<?php echo base_url();?>index.php/partida/getMovimientos",
		  type:"POST",
		  dataType: "json",
		  data:{
			partida_id  : $("#partida_id").val(),
			ganador_id  : $("#ganador_id").val(),

		  },
		  beforeSend: function () {    },
		  }).done( function(d) {
			if(d.object == 'success'){
				if(d.data != null){
					$.each( d.data, function( key, value ) {
						$("#"+value.detalle_movimiento).html(value.simbolo)
					});
				}

				$("#html_jugadora").html(d.jugadorA.nombre.toUpperCase());
				$("#html_jugadorb").html((d.jugadorB ==null)? 'ESPERANDO TU INVITADO' : d.jugadorB.nombre.toUpperCase());

				$("#estatus"    ).html(d.estatus);
				$("#estatusform").val(d.estatus);
				if(d.estatus == "TERMINADA") {$("#partidaNueva").show();}

				$("#invitado").val((d.jugadorB == null)? null : d.jugadorB.jugador_id );
				$("#num").val(d.turno);

				var turno = d.turno % 2;
					turno = (turno == 0 ) ? "X" : "O";
					$("#turno").val(turno);



			}else {
			  alert("Ha ocurrido un error en la operación");
			}
		  }).fail  ( function() { alert("Ha ocurrido un error en la operación");
		  $('#modalTusDatos').modal('hide');
		  }).always( function() {       });


	}

	// Insertar movimiento
	function insertMovimiento(movimiento, simbolo)
	{


		$.ajax({
		  url: "<?php echo base_url();?>index.php/partida/saveMovimiento",
		  type:"POST",
		  dataType: "json",
		  data:{
			movimiento  : movimiento,
			partida_id  : $("#partida_id").val(),
			jugador_id  : $("#jugador_id").val(),
			simbolo     : simbolo,
			ganador_id     : $("#ganador_id").val(),
			estatus        : $("#estatus").val(),
		  },
		  beforeSend: function () {    },
		  }).done( function(d) {
			  if(d.object == 'success'){
  			}else {
				alert("Ha ocurrido un error en la operación");
  			}
		  }).fail  ( function() { alert("Ha ocurrido un error en la operación");
		  $('#modalTusDatos').modal('hide');
		  }).always( function() {       });


	}

  	// Agrega para css al ganador
	function selectWinnerBoxes(b1, b2, b3)
	{
		b1.classList.add("win");
		b2.classList.add("win");
		b3.classList.add("win");
		turn.innerHTML = b1.innerHTML + " Felicidades!!!";
		turn.style.fontSize = "40px";
		$("#ganador_id").val($("#jugador_id").val());
		$("#estatus"   ).val('TERMINADA');
		$("#partidaNueva").show();

	}

  	// Obtenemos al ganador
	function getWinner()
	{

		var box1 = document.getElementById("box1"),
			box2 = document.getElementById("box2"),
			box3 = document.getElementById("box3"),
			box4 = document.getElementById("box4"),
			box5 = document.getElementById("box5"),
			box6 = document.getElementById("box6"),
			box7 = document.getElementById("box7"),
			box8 = document.getElementById("box8"),
			box9 = document.getElementById("box9");

		if (box1.innerHTML !== "" && box1.innerHTML === box2.innerHTML && box1.innerHTML === box3.innerHTML)
		selectWinnerBoxes(box1, box2, box3);

		if (box4.innerHTML !== "" && box4.innerHTML === box5.innerHTML && box4.innerHTML === box6.innerHTML)
		selectWinnerBoxes(box4, box5, box6);

		if (box7.innerHTML !== "" && box7.innerHTML === box8.innerHTML && box7.innerHTML === box9.innerHTML)
		selectWinnerBoxes(box7, box8, box9);

		if (box1.innerHTML !== "" && box1.innerHTML === box4.innerHTML && box1.innerHTML === box7.innerHTML)
		selectWinnerBoxes(box1, box4, box7);

		if (box2.innerHTML !== "" && box2.innerHTML === box5.innerHTML && box2.innerHTML === box8.innerHTML)
		selectWinnerBoxes(box2, box5, box8);

		if (box3.innerHTML !== "" && box3.innerHTML === box6.innerHTML && box3.innerHTML === box9.innerHTML)
		selectWinnerBoxes(box3, box6, box9);

		if (box1.innerHTML !== "" && box1.innerHTML === box5.innerHTML && box1.innerHTML === box9.innerHTML)
		selectWinnerBoxes(box1, box5, box9);

		if (box3.innerHTML !== "" && box3.innerHTML === box5.innerHTML && box3.innerHTML === box7.innerHTML)
		selectWinnerBoxes(box3, box5, box7);

	}

</script>
