<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

    <div class="container">
        <br><br><br>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <!-- <h1>Nueva Partida</h1> -->
        <p>Crea una nueva partida o  coloca el c&oacute;digo para unirte a una.</p>
        <br><br>    <br><br>
        <div class="row">
           <div class="col-md-4 col-md-offset-4 col-xs-12 text-center" >
               <div class="col-sm-12 col-xs-12">
               	<button class="btn btn-success nameJugador" type="button" data-nombre="JUGADOR 1" data-codigo="false" >&nbsp;&nbsp;  Crear Partida   &nbsp;&nbsp;<span class="glyphicon glyphicon-user"></span></button>
               	<button class="btn btn-info    nameJugador" type="button" data-nombre="JUGADOR 2" data-codigo="true"  >&nbsp;&nbsp; Unirse a Partida &nbsp;&nbsp;<span class="glyphicon glyphicon-play"></span></button>
               </div>
           </div>
        </div>
        <br><br>    <br><br>
      </div>

    </div> <!-- /container -->

    <div class="modal" tabindex="-1" role="dialog"  id="nuevaPartida">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Introduce tu nombre</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <form id="formJugador">
               <div class="form-group" id="div-nombre">
                 <label for="nombre" class="col-form-label">Nombre:</label>
                 <input type="text" class="form-control" id="nombre" name="nombre">
               </div>

               <div class="form-group" id="div-codigo" style="display:none">
                 <label for="codigo" class="col-form-label">Codigo de la Partida:</label>
                 <input type="text" class="form-control" id="codigo" name="codigo">
               </div>
              </form>
          </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-primary" id="creandoPartida">Continuar</a>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog"  id="miPartida">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Datos de tu Partida</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <form id="formJugador">
               <div class="form-group" id="div-nombre">
                    <p>
                        <label for="codigo_creado" class="col-form-label">Codigo:</label>
                        <h5  id="codigo_creado"></h5>
                    </p>

                    <p>
                        <label for="link_partida" class="col-form-label">Link Partida:</label>
                        <a id="linkPartida"><label  id="link_partida"></label></a>
                    </p>
                    <input type="hidden" class="form-control" id="link_partida" name="link_partida">

               </div>
              </form>
          </div>
          <div class="modal-footer" align="center">
            <a type="button" class="btn btn-success btn-lg btn-block" href="<?php echo base_url();?>index.php/partida">&nbsp;&nbsp;Jugar&nbsp;&nbsp;<span class="glyphicon glyphicon-play"></span></a>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">

        $( ".nameJugador" ).click(function() {
            $("#nombre").val($(this).attr('data-nombre'));
            if($(this).attr('data-codigo')  == "true"){
                $("#div-codigo").show();
            }else{
                $("#div-codigo").hide();
            }

            $("#nuevaPartida").modal('show');
        });
        $( "#creandoPartida" ).click(function() {
            $("#nuevaPartida").modal('hide');

            $.ajax({
    		  url: "<?php echo base_url();?>index.php/partida/createNuevaPartida",
    		  type:"POST",
              dataType: "json",
    		  data:{
    			nombre  : $("#nombre").val(),
    			codigo  : $("#codigo").val(),
    		  },
    		  beforeSend: function () {    },
    		  }).done( function(d) {
    			if(d.object === 'success'){
                    // alert(d.mensaje);

                    $("#codigo_creado").html(d.data.dataPartida.codigo);
                    $("#link_partida" ).html("<?php echo base_url();?>index.php/partida/jugar/"+d.data.dataJugador.jugador_id);
                    $("#jugarPartida" ).attr("href", "<?php echo base_url();?>index.php/partida/jugar/"+d.data.dataJugador.jugador_id+"/"+d.data.dataPartida.partida_id);
                    $("#linkPartida"  ).attr("href", "<?php echo base_url();?>index.php/partida/jugar/"+d.data.dataJugador.jugador_id+"/"+d.data.dataPartida.partida_id);

                    $("#miPartida").modal('show');

    			}else {
                    // alert(d.mensaje);
                    $("#nuevaPartida").modal('hide');

    			}
    		  }).fail  ( function() { alert("Ha ocurrido un error en la operación");
    		  $('#modalTusDatos').modal('hide');
    		  }).always( function() {       });

        });



    </script>
