<?php


class PartidaDetalleModel  extends  CI_Model {


    public $table             = 'partida_detalle';
    public $id                = 'partida_detalle_id';
    public $partida_id        = 'partida_id';
    public $jugador_id        = 'jugador_id';
    public $simbolo           = 'simbolo';

    public $created_at        = 'created_at';
    public $updated_at        = 'updated_at';

    public function __construct(){
        $this->load->database();
    }

    function find($id){

        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->id, $id);

        $query = $this->db->get();
        return  $query->row();

    }

    function findAll(){

        $this->db->select();
        $this->db->from($this->table);

        $query = $this->db->get();
        return  $query->result();

    }
    function findByIdPartida($partida_id){

        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->partida_id, $partida_id);

        $query = $this->db->get();
        return  $query->result();

    }
    public function create($data)
    {
        $data{'created_at'} = date('Y-m-d H:i:s');
        $data{'updated_at'} = date('Y-m-d H:i:s');

        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;

    }


}
