<?php


class PartidaModel  extends  CI_Model {


    public $table             = 'partidas';
    public $id                = 'partida_id';
    public $codigo            = 'codigo';
    public $jugador_ppal_id   = 'jugador_ppal_id';
    public $jugador_sec_id    = 'jugador_sec_id';
    public $ganador_id        = 'ganador_id';
    public $estatus           = 'estatus';
    public $start_jugar_ppal  = 'start_jugar_ppal';
    public $start_jugar_sec   = 'start_jugar_sec';
    public $updated_at        = 'updated_at';

    public function __construct(){
        $this->load->database();
    }

    function find($id)
    {

        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->id, $id);

        $query = $this->db->get();
        return  $query->row();

    }

    function findAll()
    {

        $this->db->select();
        $this->db->from($this->table);

        $query = $this->db->get();
        return  $query->result();

    }

    function findByCode($codigo)
    {

        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->codigo, $codigo);

        $query = $this->db->get();
        return  $query->row();

    }

    public function create($data)
    {
        $data{'created_at'} = date('Y-m-d H:i:s');
        $data{'updated_at'} = date('Y-m-d H:i:s');


        $this->db->insert($this->table, $data);
        $insert_id      = $this->db->insert_id();


        $data{'codigo'} = str_pad($insert_id, 6, "0", STR_PAD_LEFT);


        $this->db->update($this->table, $data, array('partida_id' => $insert_id));

        return  $insert_id;

    }

    public function update($data, $id)
    {
        $data{'updated_at'} = date('Y-m-d H:i:s');

        $this->db->update($this->table, $data, array('partida_id' => $id));

        return  $id;

    }

    public function checkEstatus($id)
    {
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->id, $id);

        $query = $this->db->get();
        $query = $query->row();
        // var_dump($query->start_jugar_sec);

        if($query->ganador_id){
            $this->estatus = 'TERMINADA';
        }
        elseif ($query->start_jugar_ppal == 1 && $query->start_jugar_sec == 1) {
            $this->estatus = 'EN PROCESO';
        }
        elseif ($query->jugador_ppal_id  && $query->jugador_sec_id ){
            $this->estatus = 'INICIADA';
        }
        else {
            $this->estatus = 'CREADA';
        }
        $data{'estatus'}    = $this->estatus;
        $data{'updated_at'} = date('Y-m-d H:i:s');

        $this->db->update($this->table, $data, array('partida_id'     => $id));

    }


}
