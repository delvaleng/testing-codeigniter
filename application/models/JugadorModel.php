<?php


class JugadorModel  extends  CI_Model {


    public $table             = 'jugadores';
    public $id                = 'jugador_id';
    public $nombre            = 'nombre';
    public $created_at        = 'created_at';
    public $updated_at        = 'updated_at';

    public function __construct(){
        $this->load->database();
    }

    function find($id){

        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->id, $id);

        $query = $this->db->get();
        return  $query->row();

    }

    function findAll(){

        $this->db->select();
        $this->db->from($this->table);

        $query = $this->db->get();
        return  $query->result();

    }

    public function create($data)
    {
        $data{'created_at'} = date('Y-m-d H:i:s');
        $data{'updated_at'} = date('Y-m-d H:i:s');

        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;

    }
}
