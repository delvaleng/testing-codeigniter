<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partida extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('PartidaModel');
		$this->load->model('JugadorModel');
		$this->load->model('PartidaDetalleModel');
	}

	// @desc: Inicio del Juego
	public function index()
	{

		$title = array(
			'title_page' => 'Inicia tu partida'
		);
		$this->load->helper('url');
		$this->load->view('layout/header', $title);
		$this->load->view('welcome');
		$this->load->view('layout/footer');

	}

	// @desc: Iniciar partida
	public function jugar($jugador_id, $partida_id)
	{

		$title = array(
			'title_page' => 'Jugar'
		);

		$partidas          = $this->PartidaModel->find($partida_id);
		$jugador           = $this->JugadorModel->find($jugador_id);


		if($jugador_id == $partidas->jugador_ppal_id){
			$llave      = 'start_jugar_ppal';
		}else {
			$llave      = 'start_jugar_sec';
		}


		$sendPartida = array(
			$llave    =>  TRUE,
			'estatus' => ($partidas->start_jugar_ppal == true && $partidas->start_jugar_sec == true )? 'EN PROCESO' : 'INICIADA'
		);
		$partida_id      = $this->PartidaModel->update($sendPartida, $partida_id);


		$data = array();
		$data['codigo']       = $partidas->codigo;

		$data['partida_id']   = $partida_id;
		$data['ganador_id']   = null;
		$data['jugador_id']   = $jugador_id;


		$data['estatus']      = $partidas->estatus;
		$data['jugador_type'] = ($partidas->jugador_ppal_id == $jugador_id)? "X" : "O";
		$data['jugada_num']   = null;
		$data['partidas']     = $partidas;



		$this->load->helper('url');
		$this->load->view('layout/header', $title);
		$this->load->view('jugar', $data);
		$this->load->view('layout/footer');

	}

	// @desc: Creamos la nueva partida o nos unimos a una
	public function createNuevaPartida()
	{
		$jugador_id = null;

		$sendJugador = array(
			'nombre'         => $this->input->post('nombre'),
		);
		$jugador_id  = $this->JugadorModel->create($sendJugador);
		if($this->input->post('codigo') == null){
			$sendPartida = array(
				'jugador_ppal_id' => $jugador_id,
			);
			$partida_id   = $this->PartidaModel->create($sendPartida);
			$dataPartida  = $this->PartidaModel->find($partida_id);

		}else {
			$sendPartida = array(
				'jugador_sec_id' => $jugador_id,
			);
			$dataPartida     = $this->PartidaModel->findByCode($this->input->post('codigo'));
			if($dataPartida){
				$partida_id      = $this->PartidaModel->update($sendPartida, $dataPartida->partida_id);
			}
		}

		$dataJugador     = $this->JugadorModel->find($jugador_id);

		$respuesta = [
			'object'    => ($jugador_id == null && $partida_id == null) ? 'error'                      : 'success',
			'mensaje'   => ($jugador_id == null && $partida_id == null) ? 'Error al crear una partida' : 'El código de tu nueva partida es: '.$dataPartida->codigo,
			'data'      => array('dataJugador' => $dataJugador, 'dataPartida' => $dataPartida)
		];
	   echo json_encode( $respuesta );
	}

	// @desc: Obtenemos los movimientos
	public function getMovimientos()
	{
		if($this->input->post('ganador_id')){
			$sendPartida = array(
				'ganador_id'    =>  $this->input->post('ganador_id'),
			);
			$partida_id      = $this->PartidaModel->update($sendPartida, $this->input->post('partida_id'));

		}
		$partidas          = $this->PartidaModel->find($this->input->post('partida_id'));
		$partidadetalles   = $this->PartidaDetalleModel->findByIdPartida($this->input->post('partida_id'));
		$turno             = count($partidadetalles);
		$this->PartidaModel->checkEstatus($this->input->post('partida_id'));


		$jugadorA          = $this->JugadorModel->find($partidas->jugador_ppal_id);
		$jugadorB          = $this->JugadorModel->find($partidas->jugador_sec_id);


		$respuesta = [
			'object'    => 'success',
			'data'      =>  $partidadetalles,
			'estatus'   =>  $partidas->estatus,
			'jugadorA'  =>  $jugadorA,
			'jugadorB'  =>  $jugadorB,
			'turno'	    =>  $turno


		];
		echo json_encode( $respuesta );

	}

	// @desc: Guardamos los movimientos
	public function saveMovimiento()
	{

		$data = array(

			'partida_id'         => $this->input->post('partida_id'),
			'jugador_id'         => $this->input->post('jugador_id'),
			'detalle_movimiento' => $this->input->post('movimiento'),
			'simbolo'            => $this->input->post('simbolo'),
		);

		if($this->input->post('estatus') == 'TERMINADA'){
			$data2 = array(

				'estatus'            => $this->input->post('estatus'),
				'ganador_id'         => $this->input->post('jugador_id'),
			);
			$partidas = $this->PartidaModel->update($data2, $this->input->post('partida_id'));

		}

		$partida_detalle_id = $this->PartidaDetalleModel->create($data);


		$respuesta = [
			'object'    => (!$partida_detalle_id) ? 'error'                   : 'success',
			'mensaje'   => (!$partida_detalle_id) ? 'Error al guardar jugada' : 'Excelente se guardo el movimiento.',
			'data'      =>  $partida_detalle_id
		];
	   echo json_encode( $respuesta );

	}

}
?>
